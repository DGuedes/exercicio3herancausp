package Model;

import java.util.ArrayList;

public class ContaCliente {
    
    private String dataCPF;
    private String dataNome;
    private String dataTelefone;
    private String dataCodigoConta;
    private String dataEndereco;
    private boolean dataInadimplente;
    private boolean dataIsContaCorrente;
    private double dataQuantiaConta;
    private ArrayList<String> listaTelefones;
    

    public double getDataQuantiaConta() {
        return dataQuantiaConta;
    }

    public void setDataQuantiaConta(double dataQuantiaConta) {
        this.dataQuantiaConta = dataQuantiaConta;
    }

    public boolean isDataIsContaCorrente() {
        return dataIsContaCorrente;
    }

    public void setDataIsContaCorrente(boolean dataIsContaCorrente) {
        this.dataIsContaCorrente = dataIsContaCorrente;
    }
    
    public String getDataCPF() {
        return dataCPF;
    }

    public void setDataCPF(String dataCPF) {
        this.dataCPF = dataCPF;
    }

    public String getDataCodigoConta() {
        return dataCodigoConta;
    }

    public void setDataCodigoConta(String dataCodigoConta) {
        this.dataCodigoConta = dataCodigoConta;
    }

    public String getDataEndereco() {
        return dataEndereco;
    }

    public void setDataEndereco(String dataEndereco) {
        this.dataEndereco = dataEndereco;
    }

    public boolean isDataInadimplente() {
        return dataInadimplente;
    }

    public void setDataInadimplente(boolean dataInadimplente) {
        this.dataInadimplente = dataInadimplente;
    }

    public String getDataNome() {
        return dataNome;
    }

    public void setDataNome(String dataNome) {
        this.dataNome = dataNome;
    }

    public String getDataTelefone() {
        return dataTelefone;
    }

    public void setDataTelefone(String dataTelefone) {
        this.dataTelefone = dataTelefone;
    }
    public ArrayList<String> getListaTelefone(){
        return listaTelefones;
    }
    public void setListaTelefone(ArrayList<String> listaTelefone){
        listaTelefones = listaTelefone;
    }
    
    
    
}
