package Operaçoes;

import Model.ContaCliente;

public class OperaçoesContaCliente {
    
    private String dataNome;
    private String dataCPF;
    private double dataQuantiaConta;
    private ContaCliente umaContaCliente;
    
    public OperaçoesContaCliente(ContaCliente umaContaCliente, String dataNome, String dataCPF, double dataQuantiaConta){
        this.dataNome = dataNome;
        this.dataCPF = dataCPF;
        this.dataQuantiaConta = dataQuantiaConta;
        this.umaContaCliente = umaContaCliente;
    }
    
    
    public void VerSaldo(String dataNome, String dataCPF){
        System.out.println(umaContaCliente.getDataQuantiaConta());
    }
    public void FazerDeposito(String dataNome, String dataCPF, double umValor){
        umaContaCliente.setDataQuantiaConta((umaContaCliente.getDataQuantiaConta()) + umValor);     
    }
    public void RetirarQuantia(String dataNome, String dataCPF, double umValor){
        umaContaCliente.setDataQuantiaConta((umaContaCliente.getDataQuantiaConta()) - umValor);     
    }
    
}
