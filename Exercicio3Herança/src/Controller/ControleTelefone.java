package Controller;

import Model.ContaCliente;
import java.util.ArrayList;

public class ControleTelefone {
    
    private String dataNome;    
    private ArrayList<String> listaTelefone = new ArrayList();
     
    
    public void AdicionarTelefone(String dataTelefone){
        listaTelefone.add(dataTelefone);
    }
    public void RemoverTelefone(String dataTelefone){
        listaTelefone.remove(dataTelefone);
    }
    public void ListarTelefones(){
        for(String umTelefone:listaTelefone){
            System.out.println(umTelefone);
        }
    }
    public ArrayList<String> RetornarTelefones(){
        return listaTelefone;
    }
}
